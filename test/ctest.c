#include <stdio.h>

struct Conta{
	int num_conta;
	int saldo;
};

struct Poupanca{
	char x;
	struct Conta conta;
};


void sacar (struct Conta* conta, int saque ){
	conta->saldo -= saque;
}



int main(){
	struct Conta c;

	c.saldo = 100;

	sacar(&c, 10);
	

	printf("%d", c.saldo);
	
	return 0;
}