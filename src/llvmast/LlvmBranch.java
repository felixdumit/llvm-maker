package llvmast;
public  class LlvmBranch extends LlvmInstruction{

	public LlvmLabelValue brTrue, brFalse, label;
	public LlvmValue cond;

    public LlvmBranch(LlvmLabelValue label){
    	this.label = label;
    }
    
    public LlvmBranch(LlvmValue cond,  LlvmLabelValue brTrue, LlvmLabelValue brFalse){
    	this.cond = cond;
    	this.brTrue = brTrue;
    	this.brFalse = brFalse;
    	this.label = null;
    }

    @Override
	public String toString(){
		if(label!= null){
			return "br label %" + label;
		}
		else{
			return "br i1 " + cond + ", label %" +  brTrue + ", label %" + brFalse;
		}

    }
}