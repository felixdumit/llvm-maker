package llvmast;


public class LlvmRegister extends LlvmValue{
	public String name;
	static int numberReg = 0;
	boolean hasName = false;

	public LlvmRegister(LlvmType type){
		this.type = type;
		this.name = "%tmp"+numberReg++;

	}

	public LlvmRegister(String name, LlvmType type){
		this.type = type;
		this.name = name;
		hasName = true;

	}

	public static void rewind(){
		numberReg = 0;
	}

	@Override
	public String toString(){
		if(hasName){
			return "%" + name; 
		}
	   return name;
	}
}
