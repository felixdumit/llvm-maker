package llvmast;
public  class LlvmIcmp extends LlvmInstruction{
    
	public LlvmRegister lhs;
	public int conditionCode;
	public LlvmType type;
	public LlvmValue op1, op2;
	
    public LlvmIcmp(LlvmRegister lhs,  int conditionCode, LlvmType type, LlvmValue op1, LlvmValue op2){
    	this.lhs = lhs;
    	this.conditionCode = conditionCode;
    	this.type = type;
    	this.op1 = op1;
    	this.op2 = op2;
    }

    @Override
	public String toString(){
    	String fun = "";
    	switch(conditionCode){
    	case 1:
    		fun = "eq";
    		break;
    	case 2:
    		fun = "ne";
    		break;
    	case 3:
    		fun = "ugt";
    		break;
    	case 4:
    		fun = "uge";
    		break;
    	case 5:
    		fun = "ult";
    		break;
    	case 6:
    		fun = "ule";
    		break;
    	case 7:
    		fun = "sgt";
    		break;
    	case 8:
    		fun = "sge";
    		break;
    	case 9:
    		fun = "slt";
    		break;
    	case 10:
    		fun = "sle";
    		break;
    	default:
    		fun = "zica";
    		break;
   
    	}
   		return "  " +lhs + " = icmp "+ fun + "  " + type + " " + op1 + ", " + op2;
    }
    
    public static final int eq = 1;
    public static final int ne = 2;
    public static final int ugt = 3;
    public static final int uge = 4;
    public static final int ult = 5;
    public static final int ule = 6;
    public static final int sgt = 7;
    public static final int sge = 8;
    public static final int slt = 9;
    public static final int sle = 10;
    

}