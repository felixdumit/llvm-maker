package llvmast;
public class LlvmCustomInstruction extends LlvmInstruction{
    public String ins;
    public LlvmCustomInstruction(String ins){
	this.ins = ins;
    }
    
    @Override
	public String toString(){
	return ins;
    }
}