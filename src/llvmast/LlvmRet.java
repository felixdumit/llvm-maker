package llvmast;
public class LlvmRet extends LlvmInstruction{
    public LlvmValue v;
    public LlvmRet(LlvmValue v){
	this.v = v;
    }

    @Override
	public String toString(){
	return "  ret " + v.type + " " + v;
    }

}