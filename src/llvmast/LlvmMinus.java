package llvmast;
// will be used even for -x
public  class LlvmMinus extends LlvmInstruction{
	public LlvmValue lhs;
    public LlvmType type;
    public LlvmValue op1, op2;

	public LlvmMinus(LlvmValue lhs, LlvmType type, LlvmValue op1, LlvmValue op2){
		this.lhs = lhs;
		this.type = type;
		this.op1 = op1;
		this.op2 = op2;
    }

    @Override
	public String toString(){
		return "  " +lhs + " = sub " + type + " " + op1 + ", " + op2;    
    }
}