package llvmast;

public class LlvmPointer extends LlvmType{
    public LlvmType content;
    
    public LlvmPointer(LlvmType content){
	this.content = content;
    }

	@Override
	public String toString(){
	return content + " *";
    }
}