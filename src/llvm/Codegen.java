/*****************************************************
Esta classe Codegen é a responsável por emitir LLVM-IR. 
Ela possui o mesmo método 'visit' sobrecarregado de
acordo com o tipo do parâmetro. Se o parâmentro for
do tipo 'While', o 'visit' emitirá código LLVM-IR que 
representa este comportamento. 
Alguns métodos 'visit' já estão prontos e, por isso,
a compilação do código abaixo já é possível.

class a{
    public static void main(String[] args){
    	System.out.println(1+2);
    }
}

O pacote 'llvmast' possui estruturas simples 
que auxiliam a geração de código em LLVM-IR. Quase todas 
as classes estão prontas; apenas as seguintes precisam ser 
implementadas: 

// llvmasm/LlvmBranch.java
// llvmasm/LlvmIcmp.java
// llvmasm/LlvmMinus.java
// llvmasm/LlvmTimes.java


Todas as assinaturas de métodos e construtores 
necessárias já estão lá. 


Observem todos os métodos e classes já implementados
e o manual do LLVM-IR (http://llvm.org/docs/LangRef.html) 
como guia no desenvolvimento deste projeto. 

 ****************************************************/
package llvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import llvmast.LlvmAlloca;
import llvmast.LlvmAnd;
import llvmast.LlvmArray;
import llvmast.LlvmBitcast;
import llvmast.LlvmBool;
import llvmast.LlvmBranch;
import llvmast.LlvmCall;
import llvmast.LlvmClassType;
import llvmast.LlvmCloseDefinition;
import llvmast.LlvmConstantDeclaration;
import llvmast.LlvmCustomInstruction;
import llvmast.LlvmDefine;
import llvmast.LlvmExternalDeclaration;
import llvmast.LlvmGetElementPointer;
import llvmast.LlvmIcmp;
import llvmast.LlvmInstruction;
import llvmast.LlvmIntegerLiteral;
import llvmast.LlvmLabel;
import llvmast.LlvmLabelValue;
import llvmast.LlvmLoad;
import llvmast.LlvmMalloc;
import llvmast.LlvmMinus;
import llvmast.LlvmNamedValue;
import llvmast.LlvmPlus;
import llvmast.LlvmPointer;
import llvmast.LlvmPrimitiveType;
import llvmast.LlvmRegister;
import llvmast.LlvmRet;
import llvmast.LlvmStore;
import llvmast.LlvmStructure;
import llvmast.LlvmTimes;
import llvmast.LlvmType;
import llvmast.LlvmValue;
import semant.Env;
import syntaxtree.And;
import syntaxtree.ArrayAssign;
import syntaxtree.ArrayLength;
import syntaxtree.ArrayLookup;
import syntaxtree.Assign;
import syntaxtree.Block;
import syntaxtree.BooleanType;
import syntaxtree.Call;
import syntaxtree.ClassDecl;
import syntaxtree.ClassDeclExtends;
import syntaxtree.ClassDeclSimple;
import syntaxtree.Equal;
import syntaxtree.Exp;
import syntaxtree.False;
import syntaxtree.Formal;
import syntaxtree.Identifier;
import syntaxtree.IdentifierExp;
import syntaxtree.IdentifierType;
import syntaxtree.If;
import syntaxtree.IntArrayType;
import syntaxtree.IntegerLiteral;
import syntaxtree.IntegerType;
import syntaxtree.LessThan;
import syntaxtree.MainClass;
import syntaxtree.MethodDecl;
import syntaxtree.Minus;
import syntaxtree.NewArray;
import syntaxtree.NewObject;
import syntaxtree.Not;
import syntaxtree.Plus;
import syntaxtree.Print;
import syntaxtree.Program;
import syntaxtree.Statement;
import syntaxtree.This;
import syntaxtree.Times;
import syntaxtree.True;
import syntaxtree.VarDecl;
import syntaxtree.VisitorAdapter;
import syntaxtree.While;

public class Codegen extends VisitorAdapter {
	private List<LlvmInstruction> assembler;
	private Codegen codeGenerator;

	private SymTab symTab;
	private ClassNode classEnv; // Aponta para a classe atualmente em uso em
								// symTab
	private MethodNode methodEnv; // Aponta para a metodo atualmente em uso em
									// symTab

	private String lastVar;

	public Codegen() {
		assembler = new LinkedList<LlvmInstruction>();
	}

	// Método de entrada do Codegen
	public String translate(Program p, Env env) {

		codeGenerator = new Codegen();

		// Preenchendo a Tabela de Símbolos
		// Quem quiser usar 'env', apenas comente essa linha
		codeGenerator.symTab = new SymTab();
		codeGenerator.symTab.FillTabSymbol(p);

		// Formato da String para o System.out.printlnijava "%d\n"
		codeGenerator.assembler.add(new LlvmConstantDeclaration(
				"@.formatting.string",
				"private constant [4 x i8] c\"%d\\0A\\00\""));

		// NOTA: sempre que X.accept(Y), então Y.visit(X);
		// NOTA: Logo, o comando abaixo irá chamar codeGenerator.visit(Program),
		// linha 75
		p.accept(codeGenerator);

		// Link do printf
		List<LlvmType> pts = new LinkedList<LlvmType>();
		pts.add(new LlvmPointer(LlvmPrimitiveType.I8));
		pts.add(LlvmPrimitiveType.DOTDOTDOT);
		codeGenerator.assembler.add(new LlvmExternalDeclaration("@printf",
				LlvmPrimitiveType.I32, pts));
		List<LlvmType> mallocpts = new LinkedList<LlvmType>();
		mallocpts.add(LlvmPrimitiveType.I32);
		codeGenerator.assembler.add(new LlvmExternalDeclaration("@malloc",
				new LlvmPointer(LlvmPrimitiveType.I8), mallocpts));

		String r = new String();
		for (LlvmInstruction instr : codeGenerator.assembler)
			r += instr + "\n";
		return r;
	}

	@Override
	public LlvmValue visit(Program n) {
		n.mainClass.accept(this);

		for (util.List<ClassDecl> c = n.classList; c != null; c = c.tail)
			c.head.accept(this);

		// System.out.println(n.getClass().toString() + " " + n.toString());
		return null;
	}

	@Override
	public LlvmValue visit(MainClass n) {

		// definicao do main
		assembler.add(new LlvmDefine("@main", LlvmPrimitiveType.I32,
				new LinkedList<LlvmValue>()));
		assembler.add(new LlvmLabel(new LlvmLabelValue("entry")));
		LlvmRegister R1 = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));
		assembler.add(new LlvmAlloca(R1, LlvmPrimitiveType.I32,
				new LinkedList<LlvmValue>()));
		assembler.add(new LlvmStore(new LlvmIntegerLiteral(0), R1));

		// Statement é uma classe abstrata
		// Portanto, o accept chamado é da classe que implementa Statement, por
		// exemplo, a classe "Print".
		n.stm.accept(this);

		// Final do Main
		LlvmRegister R2 = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmLoad(R2, R1));
		assembler.add(new LlvmRet(R2));
		assembler.add(new LlvmCloseDefinition());

		// System.out.println(n.getClass().toString() + " " + n.toString());
		return null;
	}

	@Override
	public LlvmValue visit(Plus n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue v1 = n.lhs.accept(this);
		LlvmValue v2 = n.rhs.accept(this);

		LlvmRegister lhs = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmPlus(lhs, LlvmPrimitiveType.I32, v1, v2));

		return lhs;
	}

	@Override
	public LlvmValue visit(Print n) {

		LlvmValue v = n.exp.accept(this);

		// getelementptr:
		LlvmRegister lhs = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I8));
		LlvmRegister src = new LlvmNamedValue("@.formatting.string",
				new LlvmPointer(new LlvmArray(4, LlvmPrimitiveType.I8)));
		List<LlvmValue> offsets = new LinkedList<LlvmValue>();
		offsets.add(new LlvmIntegerLiteral(0));
		offsets.add(new LlvmIntegerLiteral(0));
		List<LlvmType> pts = new LinkedList<LlvmType>();
		pts.add(new LlvmPointer(LlvmPrimitiveType.I8));
		List<LlvmValue> args = new LinkedList<LlvmValue>();
		args.add(lhs);
		args.add(v);
		assembler.add(new LlvmGetElementPointer(lhs, src, offsets));

		pts = new LinkedList<LlvmType>();
		pts.add(new LlvmPointer(LlvmPrimitiveType.I8));
		pts.add(LlvmPrimitiveType.DOTDOTDOT);

		// printf:
		assembler.add(new LlvmCall(new LlvmRegister(LlvmPrimitiveType.I32),
				LlvmPrimitiveType.I32, pts, "@printf", args));

		// System.out.println(n.getClass().toString() + " " + n.toString());
		return null;
	}

	@Override
	public LlvmValue visit(IntegerLiteral n) {
		return new LlvmIntegerLiteral(n.value);
	};

	// Todos os visit's que devem ser implementados
	@Override
	public LlvmValue visit(ClassDeclSimple n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		classEnv = symTab.classes.get(n.name.s);

		List<LlvmType> typeList = new ArrayList<LlvmType>();
		for (util.List<VarDecl> var = n.varList; var != null; var = var.tail) {
			LlvmValue v = var.head.accept(this);
			typeList.add(v.type);

		}
		for (util.List<VarDecl> var = n.varList; var != null; var = var.tail) {
			var.head.accept(this);
		}

		for (util.List<MethodDecl> var = n.methodList; var != null; var = var.tail) {
			var.head.accept(this);
		}

		LlvmStructure struct = new LlvmStructure(typeList);

		assembler.add(0, new LlvmCustomInstruction(getClassName(n.name.s)
				+ " = type " + struct.toString()));

		return null;
	}

	@Override
	public LlvmValue visit(ClassDeclExtends n) {
		System.out.println(n.getClass().toString() + " " + n.toString());
		return null;
	}

	@Override
	public LlvmValue visit(VarDecl n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		lastVar = n.name.s;

		LlvmValue type = n.type.accept(this);

		LlvmRegister r = new LlvmRegister(n.name.s, type.type);

		return r;
	}

	@Override
	public LlvmValue visit(MethodDecl n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		Map<String, MethodNode> methods = symTab.classes
				.get(classEnv.className).classMethods;
		methodEnv = methods.get(n.name.s);

		String methodName = getMethodName(classEnv.className, n.name.s);

		assembler.add(new LlvmDefine(methodName, methodEnv.retType,
				methodEnv.formals));

		for (LlvmValue formal : methodEnv.formals) {
			LlvmNamedValue lhs = new LlvmNamedValue(
					formal.toString() + ".addr", formal.type);
			assembler.add(new LlvmAlloca(lhs, formal.type,
					new LinkedList<LlvmValue>()));

			lhs = new LlvmNamedValue(formal.toString() + ".addr",
					new LlvmPointer(formal.type));

			LlvmRegister reg = new LlvmNamedValue(formal.toString(),
					formal.type);

			assembler.add(new LlvmStore(reg, lhs));

		}

		for (LlvmValue value : methodEnv.locals) {
			LlvmNamedValue lhs = new LlvmNamedValue(value.toString() + ".addr",
					value.type);

			assembler.add(new LlvmAlloca(lhs, value.type,
					new LinkedList<LlvmValue>()));
		}

		for (LlvmValue var : classEnv.varList) {
			LlvmNamedValue lhs = new LlvmNamedValue(var.toString() + ".addr",
					var.type);
			LlvmType classPtrType = new LlvmPointer(new LlvmClassType(
					classEnv.className));
			LlvmRegister src = new LlvmRegister("this", classPtrType);
			LinkedList<LlvmValue> offsets = new LinkedList<LlvmValue>();
			offsets.add(new LlvmIntegerLiteral(0));
			offsets.add(new LlvmIntegerLiteral(classEnv.varList.indexOf(var)));
			assembler.add(new LlvmGetElementPointer(lhs, src, offsets));

		}

		assembler.add(new LlvmCustomInstruction("\n"));

		for (util.List<Statement> stat = n.body; stat != null; stat = stat.tail) {
			stat.head.accept(this);
		}

		LlvmValue retExp = n.returnExp.accept(this);

		assembler.add(new LlvmRet(retExp));
		assembler.add(new LlvmCloseDefinition());

		return retExp;

	}

	@Override
	public LlvmValue visit(Formal n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue type = n.type.accept(this);

		LlvmRegister reg = new LlvmRegister(n.name.s, type.type);

		return reg;
	}

	@Override
	public LlvmValue visit(BooleanType n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmRegister booltype = new LlvmRegister(LlvmPrimitiveType.I1);
		return booltype;
	}

	@Override
	public LlvmValue visit(IntegerType n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmRegister inttype = new LlvmRegister(LlvmPrimitiveType.I32);

		return inttype;
	}

	@Override
	public LlvmValue visit(IdentifierType n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmRegister id = new LlvmRegister(new LlvmPointer(new LlvmClassType(
				n.name)));
		return id;
	}

	@Override
	public LlvmValue visit(Block n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		for (util.List<Statement> s = n.body; s != null; s = s.tail) {
			s.head.accept(this);
		}

		return null;
	}

	private int ifCounter = 0;

	@Override
	public LlvmValue visit(If n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue cond = n.condition.accept(this);
		LlvmLabelValue thenLabel = new LlvmLabelValue("ifThenLabel" + ifCounter);
		LlvmLabelValue elseLabel = new LlvmLabelValue("ifElseLabel" + ifCounter);
		LlvmLabelValue endLabel = new LlvmLabelValue("ifEndLabel" + ifCounter++);

		assembler.add(new LlvmBranch(cond, thenLabel, elseLabel));

		assembler.add(new LlvmLabel(thenLabel));
		n.thenClause.accept(this);
		assembler.add(new LlvmBranch(endLabel));

		assembler.add(new LlvmLabel(elseLabel));
		n.elseClause.accept(this);
		assembler.add(new LlvmBranch(endLabel));

		assembler.add(new LlvmLabel(endLabel));

		return null;

	}

	private int whileLabelCounter = 0;

	@Override
	public LlvmValue visit(While n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmLabelValue startLabel = new LlvmLabelValue("whileStart"
				+ whileLabelCounter);
		LlvmLabelValue whileTrueLabel = new LlvmLabelValue("whileTrueLabel"
				+ whileLabelCounter);
		LlvmLabelValue endWhileLabel = new LlvmLabelValue("endWhileLabel"
				+ whileLabelCounter++);

		assembler.add(new LlvmBranch(startLabel));
		assembler.add(new LlvmLabel(startLabel));
		LlvmValue cond = n.condition.accept(this);
		assembler.add(new LlvmBranch(cond, whileTrueLabel, endWhileLabel));
		assembler.add(new LlvmLabel(whileTrueLabel));

		n.body.accept(this);

		assembler.add(new LlvmBranch(startLabel));
		assembler.add(new LlvmLabel(endWhileLabel));

		return null;

	}

	@Override
	public LlvmValue visit(Assign n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue tmp = n.var.accept(this);

		LlvmValue lhs = new LlvmNamedValue(tmp + ".addr", new LlvmPointer(
				tmp.type));
		LlvmValue rhs = n.exp.accept(this);

		// se tipos diferents, bitcast
		if (!lhs.type.toString().equals(new LlvmPointer(rhs.type).toString())) {

			LlvmValue aux = new LlvmRegister(new LlvmPointer(rhs.type));
			assembler.add(new LlvmBitcast(aux, lhs, aux.type));
			lhs = aux;

		}

		assembler.add(new LlvmStore(rhs, lhs));

		return null;
	}

	@Override
	public LlvmValue visit(ArrayAssign n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue val = n.value.accept(this);
		LlvmValue bareIdx = n.index.accept(this);

		LlvmRegister idx = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmPlus(idx, LlvmPrimitiveType.I32, bareIdx,
				new LlvmIntegerLiteral(1)));

		LlvmValue arrayStart = n.var.accept(this);

		LlvmRegister start = new LlvmNamedValue(
				arrayStart.toString() + ".addr", new LlvmPointer(
						arrayStart.type));

		LlvmRegister src = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));
		assembler.add(new LlvmLoad(src, start));

		LlvmRegister dst = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));

		List<LlvmValue> offsets = new LinkedList<LlvmValue>();
		offsets.add(idx);
		assembler.add(new LlvmGetElementPointer(dst, src, offsets));

		assembler.add(new LlvmStore(val, dst));

		return null;
	}

	@Override
	public LlvmValue visit(Equal n) {
		System.out.println(n.getClass().toString() + " " + n.toString());
		LlvmValue lhs = n.lhs.accept(this);
		LlvmValue rhs = n.rhs.accept(this);

		LlvmRegister result = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmIcmp(result, LlvmIcmp.eq, LlvmPrimitiveType.I32,
				lhs, rhs));

		return result;
	}

	@Override
	public LlvmValue visit(And n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue v1 = n.lhs.accept(this);
		LlvmValue v2 = n.rhs.accept(this);
		LlvmRegister lhs = new LlvmRegister(LlvmPrimitiveType.I1);

		assembler.add(new LlvmAnd(lhs, lhs.type, v1, v2));

		return lhs;

	}

	@Override
	public LlvmValue visit(LessThan n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue lhs = n.lhs.accept(this);
		LlvmValue rhs = n.rhs.accept(this);

		LlvmRegister result = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmIcmp(result, LlvmIcmp.slt, LlvmPrimitiveType.I32,
				lhs, rhs));

		return result;
	}

	@Override
	public LlvmValue visit(Minus n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue lhs = n.lhs.accept(this);

		LlvmValue rhs = n.rhs.accept(this);

		LlvmRegister result = new LlvmRegister(LlvmPrimitiveType.I32);

		assembler.add(new LlvmMinus(result, LlvmPrimitiveType.I32, lhs, rhs));

		return result;

	}

	@Override
	public LlvmValue visit(Times n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue lhs = n.lhs.accept(this);

		LlvmValue rhs = n.rhs.accept(this);

		LlvmRegister result = new LlvmRegister(LlvmPrimitiveType.I32);

		assembler.add(new LlvmTimes(result, LlvmPrimitiveType.I32, lhs, rhs));

		return result;

	}

	@Override
	public LlvmValue visit(IntArrayType n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmRegister intarray = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));

		return intarray;
	}

	@Override
	public LlvmValue visit(NewArray n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue realSize = n.size.accept(this);

		LlvmRegister size = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmPlus(size, LlvmPrimitiveType.I32, realSize,
				new LlvmIntegerLiteral(1)));

		LlvmType type = n.type.accept(this).type;

		LlvmRegister reg = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));

		assembler.add(new LlvmMalloc(reg, LlvmPrimitiveType.I32, size));

		List<LlvmValue> offsets = new LinkedList<LlvmValue>();
		offsets.add(new LlvmIntegerLiteral(0));

		LlvmRegister dst = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));

		assembler.add(new LlvmGetElementPointer(dst, reg, offsets));
		assembler.add(new LlvmStore(realSize, dst));

		return reg;
	}

	@Override
	public LlvmValue visit(ArrayLookup n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue array = n.array.accept(this);

		LlvmValue bareIdx = n.index.accept(this);

		LlvmRegister idx = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmPlus(idx, LlvmPrimitiveType.I32, bareIdx,
				new LlvmIntegerLiteral(1)));

		List<LlvmValue> offsets = new LinkedList<LlvmValue>();
		offsets.add(idx);

		LlvmRegister arrayElem = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));
		assembler.add(new LlvmGetElementPointer(arrayElem, array, offsets));

		LlvmRegister lhs = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmLoad(lhs, arrayElem));

		return lhs;
	}

	@Override
	public LlvmValue visit(ArrayLength n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue array = n.array.accept(this);

		List<LlvmValue> offsets = new LinkedList<LlvmValue>();
		offsets.add(new LlvmIntegerLiteral(0));

		LlvmRegister arrayElem = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));
		assembler.add(new LlvmGetElementPointer(arrayElem, array, offsets));

		LlvmRegister lhs = new LlvmRegister(LlvmPrimitiveType.I32);
		assembler.add(new LlvmLoad(lhs, arrayElem));

		return lhs;
	}

	@Override
	public LlvmValue visit(Call n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		String className = n.object.type.toString();
		String methodName = n.method.s;
		String functionName = getMethodName(className, methodName);

		List<LlvmValue> params = new LinkedList<LlvmValue>();
		List<LlvmType> paramsType = new LinkedList<LlvmType>();

		// adiciona o this

		LlvmValue nObject = n.object.accept(this);
		LlvmType nType = n.object.type.accept(this).type;

		nObject.type = nType;

		params.add(nObject);
		paramsType.add(nType);

		// loop para outros parametros
		for (util.List<Exp> exp = n.actuals; exp != null; exp = exp.tail) {
			params.add(exp.head.accept(this));
			paramsType.add(exp.head.type.accept(this).type);

		}

		LlvmValue method = n.method.accept(this);
		LlvmType returnType = n.type.accept(this).type;
		LlvmRegister result = new LlvmRegister(returnType);

		assembler.add(new LlvmCall(result, returnType, paramsType,
				functionName, params));

		return result;
	}

	@Override
	public LlvmValue visit(True n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		return new LlvmBool(1);
	}

	@Override
	public LlvmValue visit(False n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		return new LlvmBool(0);
	}

	@Override
	public LlvmValue visit(This n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		return new LlvmRegister("this", new LlvmPointer(new LlvmClassType(
				classEnv.className)));

	}

	@Override
	public LlvmValue visit(NewObject n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmType type = n.type.accept(this).type;

		LlvmRegister reg = new LlvmRegister(type);

		assembler.add(new LlvmMalloc(reg, type, getClassName(n.className.s)));

		return reg;
	}

	@Override
	public LlvmValue visit(Not n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue lhs = n.exp.accept(this);
		LlvmIntegerLiteral zero = new LlvmIntegerLiteral(0);
		LlvmRegister reg = new LlvmRegister(LlvmPrimitiveType.I1);
		assembler.add(new LlvmIcmp(reg, LlvmIcmp.eq, LlvmPrimitiveType.I1, lhs,
				zero));

		return reg;
	}

	@Override
	public LlvmValue visit(IdentifierExp n) {
		System.out.println(n.getClass().toString() + " " + n.toString());

		LlvmValue addr = n.name.accept(this);

		LlvmRegister tmp = new LlvmRegister(addr.type);

		assembler.add(new LlvmLoad(tmp, new LlvmNamedValue(addr + ".addr",
				new LlvmPointer(addr.type))));

		return tmp;
	}

	@Override
	public LlvmValue visit(Identifier n) {

		System.out.println(n.getClass().toString() + " " + n.toString());

		lastVar = n.s;

		if (methodEnv == null) {
			return new LlvmRegister(n.s, LlvmPrimitiveType.VOID);
		}

		for (LlvmValue local : methodEnv.locals) {

			if (local.toString().replace("%", "").equals(n.s)) {
				return new LlvmRegister(n.s, local.type);

			}
		}

		for (LlvmValue formal : methodEnv.formals) {
			if (formal.toString().replace("%", "").equals(n.s)) {
				return new LlvmRegister(n.s, formal.type);
			}
		}

		for (LlvmValue var : classEnv.varList) {
			if (var.toString().replace("%", "").equals(n.s)) {
				return new LlvmRegister(n.s, var.type);
			}
		}

		return null;
	}

	public static String getClassName(String name) {
		return "%class." + name;
	}

	public static String getMethodName(String claz, String name) {
		return "@" + claz + "." + name;
	}

}
