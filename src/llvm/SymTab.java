package llvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import llvmast.LlvmArray;
import llvmast.LlvmClassType;
import llvmast.LlvmIntegerLiteral;
import llvmast.LlvmPointer;
import llvmast.LlvmPrimitiveType;
import llvmast.LlvmRegister;
import llvmast.LlvmStructure;
import llvmast.LlvmType;
import llvmast.LlvmValue;
import syntaxtree.Assign;
import syntaxtree.Block;
import syntaxtree.BooleanType;
import syntaxtree.ClassDecl;
import syntaxtree.ClassDeclExtends;
import syntaxtree.ClassDeclSimple;
import syntaxtree.Formal;
import syntaxtree.Identifier;
import syntaxtree.IdentifierType;
import syntaxtree.IntArrayType;
import syntaxtree.IntegerLiteral;
import syntaxtree.IntegerType;
import syntaxtree.MainClass;
import syntaxtree.MethodDecl;
import syntaxtree.NewArray;
import syntaxtree.Program;
import syntaxtree.Statement;
import syntaxtree.VarDecl;
import syntaxtree.VisitorAdapter;

/**********************************************************************************/
/* === Tabela de Símbolos ==== 
 * 
 * 
 */
/**********************************************************************************/

class SymTab extends VisitorAdapter {
	public Map<String, ClassNode> classes;
	private String currentClass; // aponta para a classe em uso
	private MethodNode methodEnv; // metodo em uso

	private String lastId;

	public SymTab() {
		this.classes = new HashMap<String, ClassNode>();
	}

	public LlvmValue FillTabSymbol(Program n) {
		n.accept(this);
		return null;
	}

	@Override
	public LlvmValue visit(Program n) {
		n.mainClass.accept(this);

		for (util.List<ClassDecl> c = n.classList; c != null; c = c.tail)
			c.head.accept(this);

		return null;
	}

	@Override
	public LlvmValue visit(MainClass n) {
		classes.put(n.className.s, new ClassNode(n.className.s, null, null,
				null));
		return null;
	}

	@Override
	public LlvmValue visit(ClassDeclSimple n) {

		currentClass = n.name.s;

		// Constroi TypeList com os tipos das variáveis da Classe (vai formar a
		// Struct da classe)
		List<LlvmType> typeList = new ArrayList<LlvmType>();

		// Constroi VarList com as Variáveis da Classe
		List<LlvmValue> varList = new ArrayList<LlvmValue>();
		for (util.List<VarDecl> var = n.varList; var != null; var = var.tail) {
			LlvmValue v = var.head.accept(this);
			varList.add(v);
			typeList.add(v.type);

		}

		// Percorre n.methodList visitando cada método
		Map<String, MethodNode> classMethods = new HashMap<String, MethodNode>();
		for (util.List<MethodDecl> method = n.methodList; method != null; method = method.tail) {
			method.head.accept(this);
			classMethods.put(method.head.name.s, methodEnv);
		}

		ClassNode classNode = new ClassNode(currentClass, new LlvmStructure(
				typeList), varList, classMethods);

		classes.put(n.name.s, classNode);

		return null;
	}

	@Override
	public LlvmValue visit(ClassDeclExtends n) {
		return null;
	}

	@Override
	public LlvmValue visit(VarDecl n) {

		lastId = n.name.s;
		
		LlvmValue type = n.type.accept(this);
		LlvmRegister r = new LlvmRegister(n.name.s, type.type);

		System.out.println(n.getClass().toString() + " " + n.toString());
		return r;

	}

	@Override
	public LlvmValue visit(Formal n) {
		LlvmValue type = n.type.accept(this);

		LlvmRegister reg = new LlvmRegister(n.name.s, type.type);
		System.out.println(n.getClass().toString() + " " + n.toString());

		return reg;
	}

	@Override
	public LlvmValue visit(MethodDecl n) {

		LlvmValue retType = n.returnType.accept(this);

		List<LlvmValue> argsList = new ArrayList<LlvmValue>();
		argsList.add(new LlvmRegister("this", new LlvmPointer(
				new LlvmClassType(currentClass))));
		for (util.List<Formal> formal = n.formals; formal != null; formal = formal.tail) {
			LlvmValue f = formal.head.accept(this);
			argsList.add(f);
		}

		List<LlvmValue> localList = new ArrayList<LlvmValue>();
		for (util.List<VarDecl> local = n.locals; local != null; local = local.tail) {
			LlvmValue f = local.head.accept(this);
			
			localList.add(f);
		}

		for (util.List<syntaxtree.Statement> statements = n.body; statements != null; statements = statements.tail) {
			statements.head.accept(this);
		}

		methodEnv = new MethodNode(currentClass, n.name.s, argsList, localList,
				retType.type);

		return null;

	}

	@Override
	public LlvmValue visit(Block n) {

		for (util.List<Statement> s = n.body; s != null; s = s.tail) {
			s.head.accept(this);
		}

		return null;
	}

	@Override
	public LlvmValue visit(Identifier n) {
		lastId = n.s;
		LlvmRegister id = new LlvmRegister(n.s, LlvmPrimitiveType.VOID);
		return id;
	}

	@Override
	public LlvmValue visit(IdentifierType n) {
		LlvmRegister id = new LlvmRegister(new LlvmPointer(new LlvmClassType(n.name)));
		return id;
	}

	@Override
	public LlvmValue visit(NewArray n) {

	
		LlvmValue size = n.size.accept(this);		

		return null;
	}

	@Override
	public LlvmValue visit(Assign n) {
		n.var.accept(this);
		n.exp.accept(this);
		
		return null;
	}
	
	
	@Override
	public LlvmValue visit(IntegerLiteral n) {
		return new LlvmIntegerLiteral(n.value);
	};

	@Override
	public LlvmValue visit(IntArrayType n) {

		LlvmRegister intarray = new LlvmRegister(new LlvmPointer(
				LlvmPrimitiveType.I32));

		return intarray;
	}

	@Override
	public LlvmValue visit(BooleanType n) {
		LlvmRegister booltype = new LlvmRegister(LlvmPrimitiveType.I1);
		return booltype;
	}

	@Override
	public LlvmValue visit(IntegerType n) {
		LlvmRegister inttype = new LlvmRegister(LlvmPrimitiveType.I32);

		return inttype;
	}
}

class ClassNode extends LlvmType {
	String className;
	LlvmStructure classType;
	List<LlvmValue> varList;
	Map<String, MethodNode> classMethods;

	ClassNode(String nameClass, LlvmStructure classType,
			List<LlvmValue> varList, Map<String, MethodNode> classMethods) {
		this.className = nameClass;
		this.classType = classType;
		this.varList = varList;
		this.classMethods = classMethods;
	}
}

class MethodNode {
	String methodName;
	List<LlvmValue> formals;
	List<LlvmValue> locals;
	LlvmType retType;

	MethodNode(String classN, String name, List<LlvmValue> formals,
			List<LlvmValue> locals, LlvmType retType) {
		this.methodName = name;
		this.formals = formals;
		this.locals = locals;
		this.retType = retType;

	}

}