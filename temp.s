%class.Temp = type { i32 }
@.formatting.string = private constant [4 x i8] c"%d\0A\00"
define i32 @main() {
entry:
  %tmp13 = alloca i32
  store i32 0, i32 * %tmp13
;comecoud malloc
  %tmp16 = mul i32 1, 1
  %tmp17 = call i8* @malloc ( i32 %tmp16)
  %tmp15 = bitcast i8* %tmp17 to %class.Temp*
;terminou malloc
;call start
  %tmp21 = call i32 (%class.Temp *, i32)* @Temp.start(%class.Temp * %tmp15, i32 4)
;call end
  %tmp22 = getelementptr [4 x i8] * @.formatting.string, i32 0, i32 0
;call start
  %tmp23 = call i32 (i8 *, ...)* @printf(i8 * %tmp22, i32 %tmp21)
;call end
  %tmp24 = load i32 * %tmp13
  ret i32 %tmp24
}
define i32 @Temp.start(%class.Temp * %this, i32 %y) {
  %this.addr = alloca %class.Temp *
  store %class.Temp * %this, %class.Temp * * %this.addr
  %y.addr = alloca i32
  store i32 %y, i32 * %y.addr
  %x.addr = getelementptr %class.Temp * %this, i32 0, i32 0


  store i32 2, i32 * %x.addr
  %tmp28 = load i32 * %y.addr
;call start
  %tmp31 = call i32 (%class.Temp *, i32)* @Temp.andTest(%class.Temp * %this, i32 %tmp28)
;call end
  ret i32 %tmp31
}
define i32 @Temp.add(%class.Temp * %this, i32 %b) {
  %this.addr = alloca %class.Temp *
  store %class.Temp * %this, %class.Temp * * %this.addr
  %b.addr = alloca i32
  store i32 %b, i32 * %b.addr
  %y.addr = alloca i32
  %x.addr = getelementptr %class.Temp * %this, i32 0, i32 0


  %tmp32 = load i32 * %b.addr
  %tmp33 = icmp eq  i32 %tmp32, 0
  %tmp34 = icmp eq  i1 %tmp33, 0
br i1 %tmp34, label %ifThenLabel0, label %ifElseLabel0
ifThenLabel0:
  %tmp35 = load i32 * %x.addr
  %tmp36 = load i32 * %b.addr
  %tmp37 = sub i32 %tmp35, %tmp36
  store i32 %tmp37, i32 * %y.addr
  store i32 15, i32 * %y.addr
br label %ifEndLabel0
ifElseLabel0:
  %tmp38 = load i32 * %x.addr
  %tmp39 = load i32 * %b.addr
  %tmp40 = add i32 %tmp38, %tmp39
  store i32 %tmp40, i32 * %y.addr
br label %ifEndLabel0
ifEndLabel0:
  %tmp41 = load i32 * %y.addr
  ret i32 %tmp41
}
define i32 @Temp.multiplyXBy(%class.Temp * %this, i32 %y) {
  %this.addr = alloca %class.Temp *
  store %class.Temp * %this, %class.Temp * * %this.addr
  %y.addr = alloca i32
  store i32 %y, i32 * %y.addr
  %oldX.addr = alloca i32
  %x.addr = getelementptr %class.Temp * %this, i32 0, i32 0


  %tmp42 = load i32 * %x.addr
  store i32 %tmp42, i32 * %oldX.addr
  %tmp43 = load i32 * %y.addr
  %tmp44 = sub i32 %tmp43, 2
  store i32 %tmp44, i32 * %y.addr
br label %whileStart0
whileStart0:
  %tmp45 = load i32 * %y.addr
  %tmp46 = icmp slt  i32 %tmp45, 0
  %tmp47 = icmp eq  i1 %tmp46, 0
br i1 %tmp47, label %whileTrueLabel0, label %endWhileLabel0
whileTrueLabel0:
  %tmp48 = load i32 * %y.addr
  %tmp49 = sub i32 %tmp48, 1
  store i32 %tmp49, i32 * %y.addr
  %tmp50 = load i32 * %x.addr
  %tmp51 = load i32 * %oldX.addr
  %tmp52 = add i32 %tmp50, %tmp51
  store i32 %tmp52, i32 * %x.addr
br label %whileStart0
endWhileLabel0:
  %tmp53 = load i32 * %x.addr
  ret i32 %tmp53
}
define i32 @Temp.andTest(%class.Temp * %this, i32 %y) {
  %this.addr = alloca %class.Temp *
  store %class.Temp * %this, %class.Temp * * %this.addr
  %y.addr = alloca i32
  store i32 %y, i32 * %y.addr
  %x.addr = getelementptr %class.Temp * %this, i32 0, i32 0


  store i32 3, i32 * %x.addr
  %tmp54 = load i32 * %y.addr
  %tmp55 = icmp eq  i32 %tmp54, 4
  %tmp56 = load i32 * %x.addr
  %tmp57 = icmp eq  i32 %tmp56, 5
  %tmp58 = icmp eq  i1 %tmp57, 0
  %tmp59 = and i1 %tmp55, %tmp58
br i1 %tmp59, label %ifThenLabel1, label %ifElseLabel1
ifThenLabel1:
  store i32 10, i32 * %x.addr
br label %ifEndLabel1
ifElseLabel1:
  store i32 20, i32 * %x.addr
br label %ifEndLabel1
ifEndLabel1:
  %tmp60 = load i32 * %x.addr
  ret i32 %tmp60
}
define i32 @Temp.arrayTest(%class.Temp * %this) {
  %this.addr = alloca %class.Temp *
  store %class.Temp * %this, %class.Temp * * %this.addr
  %array.addr = alloca i32 *
  %x.addr = getelementptr %class.Temp * %this, i32 0, i32 0


  ret i32 10
}
declare i32 @printf (i8 *, ...)
declare i8 * @malloc (i32)
