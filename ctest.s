; ModuleID = 'test/ctest.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-redhat-linux-gnu"

%struct.Conta = type { i32, i32 }

@.str = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: nounwind uwtable
define void @sacar(%struct.Conta* %conta, i32 %saque) #0 {
  %1 = alloca %struct.Conta*, align 8
  %2 = alloca i32, align 4
  store %struct.Conta* %conta, %struct.Conta** %1, align 8
  store i32 %saque, i32* %2, align 4
  %3 = load i32* %2, align 4
  %4 = load %struct.Conta** %1, align 8
  %5 = getelementptr inbounds %struct.Conta* %4, i32 0, i32 1
  %6 = load i32* %5, align 4
  %7 = sub nsw i32 %6, %3
  store i32 %7, i32* %5, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %c = alloca %struct.Conta, align 4
  store i32 0, i32* %1
  %2 = getelementptr inbounds %struct.Conta* %c, i32 0, i32 1
  store i32 100, i32* %2, align 4
  call void @sacar(%struct.Conta* %c, i32 10)
  %3 = getelementptr inbounds %struct.Conta* %c, i32 0, i32 1
  %4 = load i32* %3, align 4
  %5 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @.str, i32 0, i32 0), i32 %4)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
